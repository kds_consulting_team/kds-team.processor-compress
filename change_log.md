**0.0.2**
Removed verbose output for 7z and zip compressions

**0.0.1**
Working version of the processor.
Added standard compression methods such as `gz`, `7z` and `zip`.