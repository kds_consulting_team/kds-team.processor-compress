# Compress processor

This processor allows to compress files with one of the formats `gz`, `7z` or `zip`. The processor uses bash libraries to compress these files.
All of the files from `./in/files/` and `./in/tables/` folders are compressed, with exception of manifests. Compressed files are outputted to their respective `/out/` directory.

## Usage

Supports optional parameters:
- `compression` - must be one of `gz`/`gzip`, `7z` or `zip`. Default value is `gz`.
- `#password` - can be used only with `compression` = `7z`. Arbitrary string used for locking the file.

### Sample configurations

Default parameters:

```
{
  "definition": {
    "component": "kds-team.processor-compress"
  }
}
```

Choose compression type:

```
{
  "definition": {
    "component": "kds-team.processor-compress"
  },
  "parameters": {
    "compression": "gzip"
  }
}
```

Encrypt files:
```
{
  "definition": {
    "component": "kds-team.processor-compress"
  },
  "parameters": {
    "compression": "7z",
    "#password": "BigBaldWolf"
  }
}
```
