import glob
import logging
import os
import subprocess
import sys

import logging_gelf.formatters
import logging_gelf.handlers
from kbc.env_handler import KBCEnvHandler

KEY_COMPRESSION = 'compression'
FILE_PASSWORD = '#password'
MANDATORY_PARAMS = []
SUPPORTED_COMPRESSIONS = ['gz', 'zip', '7z']

APP_VERSION = '0.0.2'
sys.tracebacklimit = 0

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)-8s : [line:%(lineno)3s] %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

if 'KBC_LOGGER_ADDR' in os.environ and 'KBC_LOGGER_PORT' in os.environ:
    logger = logging.getLogger()
    logging_gelf_handler = logging_gelf.handlers.GELFTCPSocketHandler(
        host=os.getenv('KBC_LOGGER_ADDR'),
        port=int(os.getenv('KBC_LOGGER_PORT'))
    )
    logging_gelf_handler.setFormatter(logging_gelf.formatters.GELFFormatter(null_character=True))
    logger.addHandler(logging_gelf_handler)

    # removes the initial stdout logging
    logger.removeHandler(logger.handlers[0])


class Component(KBCEnvHandler):

    def __init__(self):

        KBCEnvHandler.__init__(self, MANDATORY_PARAMS)

        self.paramCompressionType = self.cfg_params.get(KEY_COMPRESSION, 'gz')
        self.paramFilePassword = self.cfg_params.get(FILE_PASSWORD)
        logging.debug("Using compression type %s." % self.paramCompressionType)

        self._validateCompression()
        self._validateEncryption()
        self._getAllFiles()
        self.run()

    def _validateCompression(self):

        if self.paramCompressionType == 'gzip':
            self.paramCompressionType = 'gz'

        if self.paramCompressionType not in SUPPORTED_COMPRESSIONS:
            logging.error(
                "Unsupported compression selected. Supported compression formats: %s." % SUPPORTED_COMPRESSIONS)
            sys.exit(1)

    def _validateEncryption(self):
        if self.paramFilePassword is not None and self.paramCompressionType != '7z':
            logging.error(
                "You can encrypt only 7z archives. Please remove the password key from the config or change the "
                "compression parameter to \"7z\".")
            sys.exit(1)

    def _getAllFiles(self):

        filesFolder = os.path.join(self.data_path, 'in/files/', '*[!.manifest]')
        tablesFolder = os.path.join(self.data_path, 'in/tables/', '*[!.manifest]')

        filesList = glob.glob(filesFolder)
        tablesList = glob.glob(tablesFolder)

        self.varFiles = filesList + tablesList
        logging.debug("All files:")
        logging.debug(self.varFiles)

    def _getDestinationFolder(self, pathToFile):

        if '/in/tables/' in pathToFile:

            return os.path.join(self.data_path, 'out/tables/')

        elif '/in/files/' in pathToFile:

            return os.path.join(self.data_path, 'out/files/')

        else:

            logging.error("Unknown path to file.")
            sys.exit(2)

    def compressGz(self):

        for f in self.varFiles:

            compressionCheck = subprocess.call(["gzip", f])

            if compressionCheck != 0:
                logging.error("Error when compressing file %s." % f)
                sys.exit(1)

            compressedName = f + '.gz'
            destinationFolder = self._getDestinationFolder(f)

            subprocess.call(["cp", compressedName, destinationFolder])

    def compressZip(self):

        for f in self.varFiles:

            destinationFolder = self._getDestinationFolder(f)
            fileName = f.split('/')[-1]

            destinationFilename = os.path.join(destinationFolder, fileName + '.zip')

            compressionCheck = subprocess.call(["zip", "-j", "-q", destinationFilename, f])

            if compressionCheck != 0:
                logging.error("Error when compressing file %s." % f)
                sys.exit(1)

    def compress7z(self):

        for f in self.varFiles:

            destinationFolder = self._getDestinationFolder(f)
            fileName = f.split('/')[-1] + '.7z'

            destinatonFilename = os.path.join(destinationFolder, fileName)

            if self.paramFilePassword:
                compressionCheck = subprocess.call(
                    ["7z", "a", destinatonFilename, "-y", "-bsp0", "-bso0", f, f"-p{self.paramFilePassword}"])
            else:
                compressionCheck = subprocess.call(["7z", "a", destinatonFilename, "-y", "-bsp0", "-bso0", f])

        if compressionCheck != 0:
            logging.error("Error when compressing file %s." % f)
            sys.exit(1)

    def run(self):

        if self.paramCompressionType == 'gz':

            self.compressGz()

        elif self.paramCompressionType == 'zip':

            self.compressZip()

        elif self.paramCompressionType == '7z':

            self.compress7z()

        else:

            logging.error("Unknown compression type %s." % self.paramCompressionType)
            sys.exit(1)


if __name__ == '__main__':
    Component()
